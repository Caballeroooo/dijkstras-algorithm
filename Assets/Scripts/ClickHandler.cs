﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class ClickHandler : MonoBehaviour
{
    private const float Distance = 10f;
    
    public static ClickHandler instance;
    
    public List<Node> _allNodes = new List<Node>();
    
    public GameObject pickPoint;
    public Node point;
    
    public bool isStopToPickPoint;
    
    [SerializeField] private Node firstClickNode;
    [SerializeField] private Node secondClickNode;

    private DijkstrasAlgorithm _algorithm;

    private Node _clickNode;

    private Vector3 _objPosition;
    private Vector3 _mousePosition;

    private bool _isCanSpawn = true;
    
    void Awake()
    {
        instance = this;
        _algorithm = new DijkstrasAlgorithm();
    }
    
    void Update()
    {
        CalculationMousePosition();

        StopToAddPointToMap();

        if (!isStopToPickPoint)
        {
            CalculationPickPointPosition();

            AddPointToMap();
        }
        
        
    }
    
    private void OnTriggerEnter2D(Collider2D col)
    {
        _isCanSpawn = false;
        _clickNode = null;
        
        if (!isStopToPickPoint)
        {
            pickPoint.SetActive(false);
        }
    }

    private void OnTriggerStay2D(Collider2D col)
    {
        if (!_clickNode)
            _clickNode = col.gameObject.GetComponent<Node>();
        
        if (isStopToPickPoint && Input.GetMouseButtonDown(0))
        {
            _clickNode = col.gameObject.GetComponent<Node>();
            if (firstClickNode == null)
                firstClickNode = _clickNode;
            else
            {
                secondClickNode = _clickNode;
                PrepareAlgorithm(firstClickNode, secondClickNode);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        _isCanSpawn = true;

        if (!isStopToPickPoint)
            pickPoint.SetActive(true);
    }

    private void CalculationMousePosition()
    {
        _mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Distance);
        _objPosition = Camera.main.ScreenToWorldPoint(_mousePosition);
        transform.position = _objPosition;
    }
    
    private void CalculationPickPointPosition()
    {
        pickPoint.transform.position = _objPosition;
    }
    
    private void AddPointToMap()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (_isCanSpawn)
            {
                Node newNode = Instantiate(point);
                newNode.transform.position = pickPoint.transform.position;
                _allNodes.Add(newNode);
                newNode.gameObject.name = "Node " + _allNodes.Count;
    
                if (_allNodes.Count > 1)
                {
                    newNode.LinkedNodes(_allNodes[_allNodes.Count - 2]);
                }
            }
            else
            {
                if (_clickNode != null)
                {
                    _allNodes.Add(_clickNode);
                    _clickNode.LinkedNodes(_allNodes[_allNodes.Count - 2]);

                    int sameNode = _allNodes.IndexOf(_clickNode);
                    _allNodes.RemoveAt(sameNode);
                }
            }
        }
    }
    
    private void StopToAddPointToMap()
    {
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            isStopToPickPoint = true;
            pickPoint.SetActive(false);
        }
    }

    private void PrepareAlgorithm(Node startNode, Node finishNode)
    {
        int indexOfStartNode = _allNodes.IndexOf(startNode);
        Node temp = _allNodes[0];
        _allNodes[0] = startNode;
        _allNodes[indexOfStartNode] = temp; 

        _algorithm.InitAlgorithm(_allNodes, finishNode);
    }
}
