﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Node : MonoBehaviour
{
    public Link linkPrefab;

    public TextMesh nodeWeightText;
    
    public List<Link> linkedNodes = new List<Link>();

    public float weightSum = float.PositiveInfinity;
    
    private bool _isColored;
    private bool _isSameLink;

    
    void Update()
    {
        nodeWeightText.text = weightSum.ToString();
    }
    
    public void LinkedNodes(Node toLink)
    {
        for (int i = 0; i < linkedNodes.Count; i++)
        {
            if ((linkedNodes[i].enterNode == this && linkedNodes[i].exitNode == toLink) ||
                (linkedNodes[i].exitNode == this && linkedNodes[i].enterNode == toLink))
                _isSameLink = true;
        }

        if (!_isSameLink)
        {
            float r = Random.Range(1, 20);
        
            Link link = Instantiate(linkPrefab);
            link.Init(this, toLink, r);
            linkedNodes.Add(link);
        
            Link linkInverse = Instantiate(linkPrefab);
            linkInverse.Init(toLink, this, r);
            toLink.linkedNodes.Add(linkInverse);
        }
    }
    
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (ClickHandler.instance.isStopToPickPoint && !_isColored)
        {
            GetComponent<SpriteRenderer>().color = Color.magenta;
        }
    }

    private void OnTriggerStay2D(Collider2D col)
    {
        if (ClickHandler.instance.isStopToPickPoint && Input.GetKeyDown(KeyCode.Mouse0))
        {
            _isColored = true;
            GetComponent<SpriteRenderer>().color = Color.red;
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (ClickHandler.instance.isStopToPickPoint && !_isColored)
        {
            GetComponent<SpriteRenderer>().color = Color.white;
        }
    }
}
