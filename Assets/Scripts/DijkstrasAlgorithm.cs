﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Object = UnityEngine.Object;

public class DijkstrasAlgorithm : MonoBehaviour
{
    private Node _firstNode;
    private Node _actualNode;
    private Node _finishNode;

    private bool _isBreak;

    public void InitAlgorithm(List<Node> _allNodes, Node fNode)
    {
        _firstNode = _allNodes[0];
        _firstNode.weightSum = 0;
        _finishNode = fNode;
        
        foreach (var node in _allNodes)
        {
            foreach (var link in node.linkedNodes)
            {
                float w = node.weightSum + link.weight;
                if (w < link.exitNode.weightSum)
                {
                    link.exitNode.weightSum = w;
                }
            }
            
            for (int i = 0; i < node.linkedNodes.Count; i++)
            {
                _actualNode = node.linkedNodes[i].exitNode;
                foreach (var link in _actualNode.linkedNodes)
                {
                    float w = _actualNode.weightSum + link.weight;
                    if (w < link.exitNode.weightSum)
                    {
                        link.exitNode.weightSum = w;
                    }
                }
            }
        }
        
        
        GetWay(_finishNode);
    }

    private void GetWay(Node newFinNode)
    {
        if (newFinNode == _firstNode)
            return;
        
        for (int i = 0; i < newFinNode.linkedNodes.Count; i++)
        {
            _actualNode = newFinNode.linkedNodes[i].exitNode;
            float weightActualNode = newFinNode.weightSum - newFinNode.linkedNodes[i].weight;
            if (weightActualNode == _actualNode.weightSum)
            {
                newFinNode.linkedNodes[i].line.GetComponent<LineRenderer>().SetColors(Color.red, Color.red);
                for (int j = 0; j < _actualNode.linkedNodes.Count; j++)
                {
                    if (_actualNode.linkedNodes[j].exitNode == newFinNode)
                    {
                        _actualNode.linkedNodes[j].line.GetComponent<LineRenderer>().SetColors(Color.red, Color.red);
                        GetWay(_actualNode);
                    }
                }
            }
        }
    }
}
    
