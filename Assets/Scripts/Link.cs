﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Link : MonoBehaviour
{
    public LineRenderer line;
    public TextMesh linkWeightText;

    public Node enterNode;
    public Node exitNode;

    public float weight;
    

    void Update()
    {
        linkWeightText.text = weight.ToString();
    }

    public void Init(Node enter, Node exit, float w)
    {
        enterNode = enter;
        exitNode = exit;
        weight = w;
        line.SetPosition(0, enterNode.transform.position);
        line.SetPosition(1, exitNode.transform.position);

        linkWeightText.transform.position = Vector3.Lerp(enterNode.transform.position, exitNode.transform.position, 0.5f);
    }
}
